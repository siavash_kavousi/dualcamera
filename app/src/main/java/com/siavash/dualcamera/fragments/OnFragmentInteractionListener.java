package com.siavash.dualcamera.fragments;

/**
 * Created by sia on 9/18/15.
 */
public interface OnFragmentInteractionListener {
    void switchFragmentTo(int index, String... optionalValues);
}
